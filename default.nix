{pkgs, ...}:
{
  home.file.".config/qutebrowser".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qutebrowser";
  home.file.".config/qutebrowser/bookmarks".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qutebrowser/bookmarks";
  home.file.".config/qutebrowser/bookmarks/urls".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qutebrowser/bookmarks/urls";
  home.file.".config/qutebrowser/quickmarks".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qutebrowser/quickmarks";
  home.file.".config/qtile".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile";
  home.file.".config/qtile/qtile_config".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config";
  home.file.".config/qtile/qtile_config/__init__.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config/__init__.py";
  home.file.".config/qtile/qtile_config/layouts.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config/layouts.py";
  home.file.".config/qtile/qtile_config/env.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config/env.py";
  home.file.".config/qtile/qtile_config/colors.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config/colors.py";
  home.file.".config/qtile/qtile_config/KeyChain.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config/KeyChain.py";
  home.file.".config/qtile/qtile_config/groups.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config/groups.py";
  home.file.".config/qtile/qtile_config/theme.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config/theme.py";
  home.file.".config/qtile/qtile_config/screens.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config/screens.py";
  home.file.".config/qtile/qtile_config/keys.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/qtile_config/keys.py";
  home.file.".config/qtile/config.py".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/config.py";
  home.file.".config/qtile/xmodmaprc".source = "/home/sam/.config/nixpkgs/dotfiles/.config/qtile/xmodmaprc";
  home.file.".config/bash".source = "/home/sam/.config/nixpkgs/dotfiles/.config/bash";
  home.file.".config/bash/key-bindings.bash".source = "/home/sam/.config/nixpkgs/dotfiles/.config/bash/key-bindings.bash";
  home.file.".config/bash/completion.bash".source = "/home/sam/.config/nixpkgs/dotfiles/.config/bash/completion.bash";
  home.file.".config/bash/alias.bash".source = "/home/sam/.config/nixpkgs/dotfiles/.config/bash/alias.bash";
  home.file.".config/bash/bashrc".source = "/home/sam/.config/nixpkgs/dotfiles/.config/bash/bashrc";
}
