# All custom files which can't be controlled with home-manager are stored under
# .config/custom/
# and linked

{pkgs, ... }:

with import <nixpkgs> {};
with builtins;
with lib;

{

  nixpkgs.config.allowUnfree = true;
  home.packages = [

  # commandlinetools
  # better cat
  pkgs.bat
  pkgs.fzf
  # cool file finding
  pkgs.fasd
  # better than man
  pkgs.tldr
  pkgs.trash-cli

  # Networking stuff
  pkgs.qutebrowser
  pkgs.wget
  pkgs.telnet
  pkgs.git
  pkgs.tdesktop # Telegram
  pkgs.nextcloud-client

  # desktop stuff
  pkgs.gimp
  pkgs.steam

  pkgs.virtualbox

  pkgs.screenkey # show pressed keys
  pkgs.slop

  # Usability stuff
  pkgs.xorg.xmodmap
  pkgs.simplescreenrecorder
  pkgs.rofi

  ];

  programs.home-manager = {
    enable = true;
    path = "https://github.com/rycee/home-manager/archive/release-18.03.tar.gz";
  };

  # The following files are all managed in my repo
  # Since I link them directly to the repo, no more nix magic is being used
  # imports = [
  #   ./dotfiles/default.nix
  # ];
}
