(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (helm-company nixos-options nix-emacs company-jedi python-django evil-collection helm-projectile projectile evil-escape evil flycheck helm helm-config ace-window tabbar-ruler tabbar org-bullets which-key try use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 3.0)))))


(setq inhibit-startup-message t)
(tool-bar-mode -1)

(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))

(package-initialize)

;; Bootstrat `use-package'
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package try
	     :ensure t)

(use-package which-key
	     :ensure t
	     :config
	     (which-key-mode))

; vim mode :D
(use-package evil
  :ensure t
  :init
  (setq evil-want-keybinding nil)
  :config
  (evil-mode 1))
; enable ctrl+u to go up in normal mode
(define-key evil-normal-state-map (kbd "C-u") 'evil-scroll-up)

; use jk instead of Escape to exit insert mode
(use-package evil-escape
  :ensure t)
(evil-escape-mode 1)
(setq-default evil-escape-key-sequence "jk")


; show big numbers when moving between 3+ windows
(use-package ace-window
  :ensure t
  :init
  (progn
    (global-set-key [remap other-window] 'ace-window)
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-face-foreground :height 3.0)))))
    ))

;; Org-mode stuff
(use-package org-bullets
  :ensure t
  :config
  (add-hook 'helm-org-mode-hook (lambda () (org-bullets-mode 1))))

; autocomplete
(use-package company
:ensure t
:config
(setq company-idle-delay 0)
(setq company-minimum-prefix-length 3)
(global-company-mode t))

; Necessary to automatically display something
(evil-declare-change-repeat 'company-complete)

(use-package helm-company
  :ensure t)

; Tabbar on top
;(use-package tabbar-ruler
;  :ensure t
;  :config)

  
(use-package helm
  :ensure t)

;(require 'helm-config)
(global-set-key (kbd "M-x") 'helm-M-x)
(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
(global-set-key (kbd "C-x C-f") #'helm-find-files)
(helm-mode 1)

;easyfind for emacs
(use-package avy
  :ensure t
  :init
  (define-key evil-normal-state-map (kbd "f") 'avy-goto-char))


;code checker
(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode))

; python checker
(use-package company-jedi             ;;; company-mode completion back-end for Python JEDI
  :ensure t
  :config)
  ;(add-hook 'python-mode-hook 'jedi:setup)

;(setq jedi:environment-virtualenv (list (expand-file-name "~/.emacs.d/.python-environments/")))
(setq jedi:complete-on-dot t)

(defun config/enable-company-jedi ()
(add-to-list 'company-backends 'company-jedi))
(add-hook 'python-mode-hook 'config/enable-company-jedi)
; (use-package anaconda-mode
;   :ensure t
;   :config
;   (progn
;     (add-hook 'python-mode-hook 'anaconda-mode)
;     (add-hook 'python-mode-hook 'anaconda-eldoc-mode)))
;
; autocomplete python stuff
;(use-package company-anaconda
;  :ensure t
;  :after 'company
;  :config
;  (add-to-list 'company-backends 'company-anaconda))

(use-package projectile
  :ensure t
  :init
  (progn
    (setq projectile-completion-system 'avy)
    (projectile-mode 1)
    (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)))

(use-package helm-projectile
  :ensure t
  :init
    (helm-projectile-on))
(setq projectile-project-search-path '("~/projects/" "/ssh:locpa:/srv/dev/" "~/.config/"))

(use-package evil-collection
  :after evil
  :ensure t
  :config
  (evil-collection-init))


; remote shells don't use nixos
(defun my/shell-set-hook ()
  (when (file-remote-p (buffer-file-name))
    (let ((vec (tramp-dissect-file-name (buffer-file-name))))
     ;; Please change "some-hostname" to your remote hostname
      (when (string-match-p "locpa" (tramp-file-name-host vec))
        (setq-local shell-file-name "/usr/bin/bash")))))

(add-hook 'find-file-hook #'my/shell-set-hook)

(use-package python-django
  :ensure python-django)

(use-package nixos-options
  :ensure t
  :init)

;(use-package nix-sandbox
;  :ensure t
;  :config
;  (setq flycheck-command-wrapper-function
;        (lambda (command) (apply 'nix-shell-command (nix-current-sandbox) command))
;      flycheck-executable-find
;        (lambda (cmd) (nix-executable-find (nix-current-sandbox) cmd))))

(provide 'init)
;;; init.el ends here
