from libqtile.config import Key
from libqtile.widget.textbox import TextBox
from libqtile import bar
from libqtile import xcbq
from libqtile.command import lazy
from xcffib.xproto import EventMask
import xcffib
import subprocess

class KeyNode(Key):
    def __init__(self,
                 modifiers,
                 key,
                 children,
                 *commands,
                 ishome=False,
                 **kwds):
        super().__init__(modifiers, key, *commands,
                         lazy.function(self.updateMap()), **kwds)
        if commands:
            self.iscommand = True
        else:
            self.iscommand = False

        if ishome:
            self.home = self
            self.key = " "
        elif 'name' in kwds:
            self.key = kwds.get('name')
        else:
            self.key = key
        self.ishome = ishome
        self.children = children

    def addchildren(self, *children):
        for child in children:
            self.children.append(child)
            if hasattr(self, 'home'):
                child.sethome(self.home)

    def sethome(self, key):
        if not hasattr(self, 'home'):
            self.home = key
        if not self.iscommand:
            for child in self.children:
                child.sethome(self.home)

    def _auto_modmasks(self):
        yield 0
        yield xcbq.ModMasks["lock"]
        if self.numlockMask:
            yield self.numlockMask
            yield self.numlockMask | xcbq.ModMasks["lock"]

    # try to get a keyrelease thingylithing but it aint work somehow =(
    # Delete if not necessary anymore
    def betterupdate(self, qtile):
        qtile.ignoreEvents.remove(xcffib.xproto.KeyReleaseEvent)
        keylist = qtile.keyMap.copy()
        for key in keylist:
            qtile.unmapKey(qtile.keyMap[key])

        for child in self.children:
            qtile.keyMap[(child.keysym, child.modmask & qtile.validMask)] = child
            code = qtile.conn.keysym_to_keycode(child.keysym)
            for amask in qtile._auto_modmasks():
                qtile.root.grab_button(
                    code,
                    child.modmask | amask,
                    True,
                    EventMask.ButtonRelease,
                    xcffib.xproto.GrabMode.Async,
                    xcffib.xproto.GrabMode.Async,
                )


    def update(self, qtile):
        keylist = qtile.keyMap.copy()
        for key in keylist:
            qtile.unmapKey(qtile.keyMap[key])

        if self.iscommand and hasattr(self, 'home'):
            n = self.home
        else:
            n = self

        for w in Keywidget.instances:
            w.update(n.key)

        for child in n.children:
            qtile.mapKey(child)
        if not n.ishome and hasattr(n, 'home'):
            qtile.mapKey(n.home)

    def updateMap(self):
        def f(qtile):
            self.update(qtile)
        return f


class Keywidget(TextBox):
    instances = []
    """Shows number of available updates

    Needs the pacman package manager installed. So will only work in Arch Linux
    installation.
    """
    def __init__(self, text=" ", width=bar.CALCULATED, **config):
        super().__init__(text, width, **config)
        Keywidget.instances.append(self)
