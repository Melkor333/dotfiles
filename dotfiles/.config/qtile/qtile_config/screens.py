from libqtile import bar, widget
from libqtile.config import Screen
from .KeyChain import Keywidget

from .env import num_screens, hostname
from .theme import *

screens= []

fake_screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_dark),
                widget.Prompt(**default_config_bright),
                widget.GroupBox(**group_configuration),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-5, **fg_bright),
                widget.TextBox(text="CPU", **default_config_dark),
                widget.CPUGraph(
                    core="all",
                    frequency=5,
                    line_width=2,
                    border_width=1,
                    background=col_dark),
                widget.TextBox(text="  MEM", **default_config_dark),
                widget.Memory(
                    fmt="{MemUsed}M",
                    update_interval=10,
                    background=col_dark),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_dark),
                widget.TextBox(text="  BAT", **default_config_bright),
                # widget.BatteryIcon(background=col_dark),
                widget.Battery(
                    battery_name="BAT0",
                    charge_char="▲",
                    discharge_char="▼",
                    format="{percent:2.0%} {char}",
                    background=col_bright),
                widget.TextBox(text="  VOL", **default_config_bright),
                widget.Volume(channel="Master", background=col_bright),
                #widget.Wlan(interface='wlp0s20u1', background=col_bright),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_bright),
                widget.WindowName(**default_config_dark),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_dark),
                widget.Clock(
                    **default_config_bright, format='%Y-%m-%d %a %I:%M %p'),
                widget.TextBox(
                    text="◤ ",
                    fontsize=60,
                    padding=-5,
                    foreground=col_bright,
                    background=col_dark),
            ],
            24,
        ),
        x=0,
        y=0,
        width=1720,
        height=1440,),
    Screen(
        top=bar.Bar(
            [
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_dark),
                widget.Prompt(**default_config_bright),
                widget.GroupBox(**group_configuration),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-5, **fg_bright),
                widget.TextBox(text="CPU", **default_config_dark),
                widget.CPUGraph(
                    core="all",
                    frequency=5,
                    line_width=2,
                    border_width=1,
                    background=col_dark),
                widget.TextBox(text="  MEM", **default_config_dark),
                widget.Memory(
                    fmt="{MemUsed}M",
                    update_interval=10,
                    background=col_dark),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_dark),
                widget.TextBox(text="  BAT", **default_config_bright),
                # widget.BatteryIcon(background=col_dark),
                widget.Battery(
                    battery_name="BAT0",
                    charge_char="▲",
                    discharge_char="▼",
                    format="{percent:2.0%} {char}",
                    background=col_bright),
                widget.TextBox(text="  VOL", **default_config_bright),
                widget.Volume(channel="Master", background=col_bright),
                #widget.Wlan(interface='wlp0s20u1', background=col_bright),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_bright),
                widget.WindowName(**default_config_dark),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_dark),
                widget.Clock(
                    **default_config_bright, format='%Y-%m-%d %a %I:%M %p'),
                widget.TextBox(
                    text="◤ ",
                    fontsize=60,
                    padding=-5,
                    foreground=col_bright,
                    background=col_dark),
            ],
            24,
        ),
        x=1720,
        y=0,
        width=1720,
        height=1440,
        ),
    Screen(
        bottom=bar.Bar(
            [
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-5, **fg_dark),
                widget.GroupBox(**group_configuration),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-5, **fg_bright),
                widget.Prompt(**default_config_dark),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-5, **fg_dark),
                widget.TextBox(
                    "Vol: ", name="volume", **default_config_bright),
                widget.Volume(channel="Master", background=col_bright),
                #widget.Wlan(interface='wlp5s0', background=col_bright),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_bright),
                widget.WindowName(**default_config_dark),
                widget.TextBox(
                    text="◤ ", fontsize=60, padding=-0, **fg_dark),
                widget.Clock(
                    **default_config_bright, format='%Y-%m-%d %a %I:%M %p'),
                widget.TextBox(
                    text="◤ ",
                    fontsize=60,
                    padding=-5,
                    foreground=col_bright,
                    background=col_dark),
                #widget.Notify(),
                #widget.Systray(),
                #widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
            ],
            24,
        ),
        x=3440,
        y=540,
        width=1440,
        height=900,
        ),
]
# if num_screens[hostname] == 2:
#     fake_screens = [
#         Screen(
#             bottom=bar.Bar(
#                 [
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_dark),
#                     widget.Prompt(**default_config_bright),
#                     widget.GroupBox(**group_configuration),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-5, **fg_bright),
#                     widget.TextBox(text="CPU", **default_config_dark),
#                     widget.CPUGraph(
#                         core="all",
#                         frequency=5,
#                         line_width=2,
#                         border_width=1,
#                         background=col_dark),
#                     widget.TextBox(text="  MEM", **default_config_dark),
#                     widget.Memory(
#                         fmt="{MemUsed}M",
#                         update_interval=10,
#                         background=col_dark),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_dark),
#                     widget.TextBox(text="  BAT", **default_config_bright),
#                     # widget.BatteryIcon(background=col_dark),
#                     widget.Battery(
#                         battery_name="BAT0",
#                         charge_char="▲",
#                         discharge_char="▼",
#                         format="{percent:2.0%} {char}",
#                         background=col_bright),
#                     widget.TextBox(text="  VOL", **default_config_bright),
#                     widget.Volume(channel="Master", background=col_bright),
#                     #widget.Wlan(interface='wlp0s20u1', background=col_bright),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_bright),
#                     widget.WindowName(**default_config_dark),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_dark),
#                     widget.Clock(
#                         **default_config_bright, format='%Y-%m-%d %a %I:%M %p'),
#                     widget.TextBox(
#                         text="◤ ",
#                         fontsize=60,
#                         padding=-5,
#                         foreground=col_bright,
#                         background=col_dark),
#                 ],
#                 24,
#             ),
#             x=0,
#             y=0,
#             width=1720,
#             height=1440,),
#         Screen(
#             top=bar.Bar(
#                 [
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_dark),
#                     widget.Prompt(**default_config_bright),
#                     widget.GroupBox(**group_configuration),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-5, **fg_bright),
#                     widget.TextBox(text="CPU", **default_config_dark),
#                     widget.CPUGraph(
#                         core="all",
#                         frequency=5,
#                         line_width=2,
#                         border_width=1,
#                         background=col_dark),
#                     widget.TextBox(text="  MEM", **default_config_dark),
#                     widget.Memory(
#                         fmt="{MemUsed}M",
#                         update_interval=10,
#                         background=col_dark),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_dark),
#                     widget.TextBox(text="  BAT", **default_config_bright),
#                     # widget.BatteryIcon(background=col_dark),
#                     widget.Battery(
#                         battery_name="BAT0",
#                         charge_char="▲",
#                         discharge_char="▼",
#                         format="{percent:2.0%} {char}",
#                         background=col_bright),
#                     widget.TextBox(text="  VOL", **default_config_bright),
#                     widget.Volume(channel="Master", background=col_bright),
#                     #widget.Wlan(interface='wlp0s20u1', background=col_bright),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_bright),
#                     widget.WindowName(**default_config_dark),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_dark),
#                     widget.Clock(
#                         **default_config_bright, format='%Y-%m-%d %a %I:%M %p'),
#                     widget.TextBox(
#                         text="◤ ",
#                         fontsize=60,
#                         padding=-5,
#                         foreground=col_bright,
#                         background=col_dark),
#                 ],
#                 24,
#             ),
#             x=1720,
#             y=0,
#             width=1720,
#             height=1440,
#             ),
#         Screen(
#             bottom=bar.Bar(
#                 [
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-5, **fg_dark),
#                     widget.GroupBox(**group_configuration),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-5, **fg_bright),
#                     widget.Prompt(**default_config_dark),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-5, **fg_dark),
#                     widget.TextBox(
#                         "Vol: ", name="volume", **default_config_bright),
#                     widget.Volume(channel="Master", background=col_bright),
#                     #widget.Wlan(interface='wlp5s0', background=col_bright),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_bright),
#                     widget.WindowName(**default_config_dark),
#                     widget.TextBox(
#                         text="◤ ", fontsize=60, padding=-0, **fg_dark),
#                     widget.Clock(
#                         **default_config_bright, format='%Y-%m-%d %a %I:%M %p'),
#                     widget.TextBox(
#                         text="◤ ",
#                         fontsize=60,
#                         padding=-5,
#                         foreground=col_bright,
#                         background=col_dark),
#                     #widget.Notify(),
#                     #widget.Systray(),
#                     #widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
#                 ],
#                 24,
#             ), ),
#     ]
# else:
#     if hostname != 'Waluigi':
#         screens = [
#             Screen(
#                 bottom=bar.Bar(
#                     [
#                         widget.TextBox(
#                             text="◤ ", fontsize=60, padding=-5, **fg_dark),
#                         widget.GroupBox(**group_configuration),
#                         widget.TextBox(
#                             text="◤ ", fontsize=60, padding=-5, **fg_bright),
#                         Keywidget(**default_config_dark),
#                         widget.Prompt(**default_config_dark),
#                         widget.TextBox(
#                             text="◤ ", fontsize=60, padding=-5, **fg_dark),
#                         widget.TextBox(
#                             "Vol: ", name="volume", **default_config_bright),
#                         widget.Volume(channel="Master", background=col_bright),
#                         #widget.Wlan(interface='wlp5s0', background=col_bright),
#                         widget.TextBox(
#                             text="◤ ", fontsize=60, padding=-0, **fg_bright),
#                         widget.WindowName(**default_config_dark),
#                         widget.TextBox(
#                             text="◤ ", fontsize=60, padding=-0, **fg_dark),
#                         widget.Clock(
#                             **default_config_bright, format='%Y-%m-%d %a %I:%M %p'),
#                         widget.TextBox(
#                             text="◤ ",
#                             fontsize=60,
#                             padding=-5,
#                             foreground=col_bright,
#                             background=col_dark),
#                         # widget.Notify(),
#                         # widget.Systray(),
#                         # widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
#                     ],
#                     24,
#                 ), ),
#         ]
#     else:
#         screens = [
#             Screen(
#                 bottom=bar.Bar(
#                     [
#                         widget.TextBox(
#                             text="◤ ", fontsize=60, padding=-5, **fg_dark),
#                         widget.GroupBox(**group_configuration),
#                         #widget.TextBox(
#                         #    text="◤ ", fontsize=60, padding=-5, **fg_bright),
#                         #widget.Prompt(**default_config_dark),
#                         #widget.WindowName(**default_config_dark),
#                         widget.TextBox(
#                             text="◤ ", fontsize=60, padding=-0, **fg_dark),
#                         widget.Clock(
#                             **default_config_bright, format='%Y-%m-%d %a %I:%M %p'),
#                         widget.TextBox(
#                             text="◤ ",
#                             fontsize=60,
#                             padding=-5,
#                             foreground=col_bright,
#                             background=col_dark),
#                         # widget.Notify(),
#                         # widget.Systray(),
#                         # widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
#                     ],
#                     24,
#                 ), ),
#         ]
# 
