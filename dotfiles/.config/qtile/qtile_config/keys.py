from libqtile.config import hook
import subprocess

from libqtile.config import Drag, Click, Key
from libqtile.command import lazy

from .KeyChain import KeyNode
from .env import mod, returnkey, hostname
from .groups import groups


mkeys = {}

mousemod = "mod1"

# set the custom keymap so that caps can be used with "Super_R"
@hook.subscribe.startup_once
def set_keymap():
    subprocess.run(['/home/sam/.nix-profile/bin/xmodmap', '/home/sam/.config/qtile/xmodmaprc'])

root = KeyNode([], "Super_R", [], name="Root")
home = KeyNode([], returnkey, [], ishome=True)
home.addchildren(
    root,
    KeyNode([mousemod, 'shift'], 'r', [], lazy.restart()),
    KeyNode([mousemod, "shift"], 'q', [], lazy.shutdown()),
)
root.sethome(home)
keys = home.children

design_keys = KeyNode([], "d", [
    KeyNode([], 'd', [], lazy.spawn('wpg -m')),
    KeyNode([], 'e', [], lazy.spawn('wpg')),
], name="design")

qtile_keys = KeyNode([], "q", [
    KeyNode([], 'r', [], lazy.restart()),
    KeyNode(["shift"], 'q', [], lazy.shutdown()),
    KeyNode([], "space", [], lazy.next_layout()),
    KeyNode([], 'e', [], lazy.spawn('emacs ~/.config/qtile/qtile_config/keys.py')),
], name="qtile")

window_keys = KeyNode([], "w", [
    KeyNode([], "w", [], lazy.spawn('rofi -show window')),
    # Move windows, this way only works with plasma layout
    # KeyNode([], "j", [], lazy.layout.move_down()),
    # KeyNode([], "k", [], lazy.layout.move_up()),
    # KeyNode([], "h", [], lazy.layout.move_left()),
    # KeyNode([], "l", [], lazy.layout.move_right()),

    KeyNode([], "j", [], lazy.layout.shuffle_down()),
    KeyNode([], "k", [], lazy.layout.shuffle_up()),
    KeyNode([], "h", [], lazy.layout.shuffle_left()),
    KeyNode([], "l", [], lazy.layout.shuffle_right()),

    # Change layout
    KeyNode(["control"], "j", [], lazy.layout.integrate_down(), lazy.window.disable_floating()),
    KeyNode(["control"], "k", [], lazy.layout.integrate_up(), lazy.window.disable_floating()),
    KeyNode(["control"], "h", [], lazy.layout.integrate_left(), lazy.window.disable_floating()),
    KeyNode(["control"], "l", [], lazy.layout.integrate_right(), lazy.window.disable_floating()),

    # Nodeesize
    KeyNode(["shift"], "j", [], lazy.layout.grow_height(-30)),
    KeyNode(["shift"], "k", [], lazy.layout.grow_height(30)),
    KeyNode(["shift"], "h", [], lazy.layout.grow_width(-30)),
    KeyNode(["shift"], "l", [], lazy.layout.grow_width(30)),

    # Delete window
    KeyNode(["shift"], "d", [], lazy.window.kill()),
], name="windows")

group_keys = KeyNode([], "g", [], name='qtile_group')
for group in groups:
    group_keys.addchildren(
        KeyNode([], group.name, [], lazy.group[group.name].toscreen()),
        KeyNode(["shift"], group.name, [], lazy.window.togroup(group.name)),
    )

project_keys = KeyNode([], "p", [], name='projects')

open_keys = KeyNode([], 'o', [
    KeyNode([], 'q', [], lazy.spawn("qutebrowser")),
    KeyNode([], 'x', [], lazy.spawn("xterm")),
    KeyNode([], 'o', [], lazy.spawn('rofi -show run')),
], name='open')

sound_keys = KeyNode([], 's', [
    KeyNode([], 's', [], lazy.spawn('pavucontrol-qt')),
    KeyNode([], 'r', [], lazy.spawn('qjackctl'), lazy.spawn('pavucontrol-qt'), lazy.spawn('ardour5')),
], name='sound')


nixos_keys = KeyNode([], "n", [
    KeyNode([], 'e', [], lazy.spawn('xterm -e "nvim ~/.config/nixpkgs/home.nix"')),
    KeyNode([], 'c', [], lazy.spawn('xterm -e "sudo nvim /etc/nixos/configuration.nix"')),
], name='nixos')

root.addchildren(
    KeyNode([], "space", [], lazy.next_screen()),
    KeyNode(["shift"], "space", [], lazy.prev_screen()),
    design_keys, # d
    group_keys, # g
    nixos_keys, # n
    open_keys, # o
    project_keys, # p
    qtile_keys, # q
    sound_keys, # s
    window_keys, # w
    # Backupkeys
)

root.addchildren(
    # Switch between windows
    KeyNode([], "j", [], lazy.layout.down()),
    KeyNode([], "k", [], lazy.layout.up()),
    KeyNode([], "h", [], lazy.layout.left()),
    KeyNode([], "l", [], lazy.layout.right()),

    # KeyNode([], "space", [], lazy.spawn("cool-retro-term")),
    # KeyNode([], "Return", [], lazy.spawn("oni")),

    # TNodeoggle between different layouts as defined below
    # KeyNode([], "Tab", [], lazy.next_layout()),
)



# The default bindings
mkeys["mod4"] = [

    # Switch between windows
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),

    # Move windows
    Key([mod, "shift"], "j", lazy.layout.move_down(), lazy.window.disable_floating()),
    Key([mod, "shift"], "k", lazy.layout.move_up()),
    Key([mod, "shift"], "h", lazy.layout.move_left()),
    Key([mod, "shift"], "l", lazy.layout.move_right()),

    # Change layout
    Key([mod, "control"], "j", lazy.layout.integrate_down()),
    Key([mod, "control"], "k", lazy.layout.integrate_up()),
    Key([mod, "control"], "h", lazy.layout.integrate_left()),
    Key([mod, "control"], "l", lazy.layout.integrate_right()),
    Key([mod, "mod1"], "h", lazy.next_screen()),
    Key([mod, "mod1"], "l", lazy.prev_screen()),

    # Resize
    Key([mod, "control", "shift"], "j", lazy.layout.grow_height(-30)),
    Key([mod, "control", "shift"], "k", lazy.layout.grow_height(30)),
    Key([mod, "control", "shift"], "h", lazy.layout.grow_width(-30)),
    Key([mod, "control", "shift"], "l", lazy.layout.grow_width(30)),

    # multiple stack panes
    # Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    Key([mod], "space", lazy.spawn("cool-retro-term")),
    Key([mod], "Return", lazy.spawn("oni")),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    # Key([mod], "w", lazy.window.kill()),
    # Key([mod, "control"], "r", lazy.restart()),
    # Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod], "m", lazy.spawncmd()),
]

# keys = mkeys[mod]
# for child in home.children:
#     keys.extend([child])

if hostname == "Mario":
    keys = mkeys["mod4"]
    for i in groups:
        keys.extend([
            # mod1 + letter of group = switch to group
            Key([mod], i.name, lazy.group[i.name].toscreen()),

            # mod1 + shift + letter of group = switch to &
            # move focused window to group
            Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
        ])

# Drag floating layouts.
mouse = [
    Drag(
        [mousemod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position()),
    Drag(
        [mousemod, "shift"],
        "Button1",
        lazy.window.set_size_floating(),
        start=lazy.window.get_size()),
    Click([mousemod, "control"], "Button1", lazy.window.bring_to_front())
]
