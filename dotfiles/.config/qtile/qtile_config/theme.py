from . import colors

# col_bright = ["33393B", "232729"]
# col_dark= ["232729", "292A2A"]
col_bright = [colors.fg1, colors.fg2]
col_dark = [colors.fg2, colors.fg1]
col_warning = [colors.color1]

fg_dark = dict(foreground=col_dark, background=col_bright)
fg_bright = dict(foreground=col_bright, background=col_dark)

widget_defaults = dict(
    font='DejaVuSansMonoForPowerline Nerd Font',
    fontsize=12,
    padding=3,
)
default_config_dark = dict(
    fontsize=12,
    foreground=colors.active,
    background=col_dark,
    font="ttf-droid",
    margin_y=2,
    font_shadow="000000",
)
default_config_bright = dict(
    fontsize=12,
    foreground=colors.active,
    background=col_bright,
    font="ttf-droid",
    margin_y=2,
    font_shadow="000000",
)
group_configuration = dict(
    active=colors.active,
    inactive=colors.inactive,
    fontsize=12,
    background=col_bright,
    font="ttf-droid",
    margin_y=2,
    font_shadow="000000")

extension_defaults = widget_defaults.copy()
