import os
import platform


hostname = platform.node()
#hostname = "xephyr"
# Xephyr should run as display9 for this to work
if os.environ['DISPLAY'] == ':9':
    hostname = 'xephyr'

if hostname == 'Mario':
    mod = "mod4"
else:
    mod = "mod1"

returnkey = "Escape"

num_screens = {
    'Mario': 2,
    'xephyr': 1,
    'Waluigi':2,
}
