from libqtile.config import hook
from libqtile import layout

# ignore warnings
from qtile_config.groups import groups
from qtile_config.keys import keys, mouse
from qtile_config.layouts import layouts, floating_layout
from qtile_config.screens import screens, fake_screens

import subprocess


# startup apps
def wallpaper():
    subprocess.run(['wpg', '-m'])

def nmapplet():
    subprocess.Popen(['nm-applet'])

def set_xrandr():
    subprocess.run(['/home/sam/.screenlayout/default.sh'])

def set_modmap():
    subprocess.run(['xmodmap /home/sam/.config/qtile/xmodmaprc'])

@hook.subscribe.startup_once
def autostart():
#    nmapplet()
    # is being done automatically i guess from xrandr but not sure lmao
    # is being made by wpg (wpgtk)
    wallpaper()

@hook.subscribe.startup
def startup():
    set_xrandr()
    set_modmap()


dgroups_key_binder = None
dgroups_app_rules = []
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, github issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "qtile"

# vim: tabstop=4 shiftwidth=4 expandtab

# @hook.subscribe.client_new
# def jack_connection_kit(window):
#     if(window.window.get_name() == 'JACK Audio Connection Kit [Scarlett] Started.'):
#         window.floating = True
#         window.width=800
#         window.height=150


# floating_layout = layout.Floating(float_rules=[
#     {"wmname": "qjackctl"},
#     ])
