# I3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

set $mod Mod4
# Never use alt - it's a program key
set $super Hyper_L

#set $super Mod3
set $coloract "#6a8e7a"
set $colorin "#3c5045"
set_from_resource $white i3wm.color15 "#fefbec"
set_from_resource $split i3wm.color10 "#9E664B0"
#
## Font for window titles. Will also be used by the bar unless a different font
## is used in the bar {} block below.
font pango:monospace 8
#
gaps outer 0
gaps inner 8
new_window pixel 3

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+q exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
bindsym $mod+q exec --no-startup-id qutebrowser, mode "default"

# start a terminal
#bindsym $mod+t exec --no-startup-id i3-sensible-terminal
bindsym $mod+Return exec --no-startup-id i3-sensible-terminal

# exec keybindings
bindsym --release Print exec scrot
bindsym --release Shift+Print exec scrot -s

bindsym $super mode "$top"

mode "$top" {
    bindsym Shift+r reload, mode "default"
    # change focus
    bindsym h focus left, mode "default"
    bindsym j focus down, mode "default"
    bindsym k focus up, mode "default"
    bindsym l focus right, mode "default"
    # move focused window
    bindsym Shift+h move left, mode "default"
    bindsym Shift+j move down, mode "default"
    bindsym Shift+k move up, mode "default"
    bindsym Shift+l move right, mode "default"

    # focus the parent container
    bindsym Shift+p focus parent, mode "default"

    # focus the child container
    bindsym Shift+d focus child, mode "default"

    # toggle tiling / floating
    bindsym Shift+space floating toggle, mode "default"

    # enter fullscreen mode for the focused container
    bindsym Control+space fullscreen toggle, mode "default"

    # change focus between tiling / floating windows
    bindsym space focus mode_toggle, mode "default"

    bindsym w mode "$window"
    bindsym o mode "$open"
    bindsym d mode "$design"
    bindsym q mode "$windowmanager"
    bindsym g mode "$group"
    bindsym $super mode "default"


    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}




mode "$open" {
    bindsym o exec --no-startup-id rofi -show run, mode "default"
    # Ranger should come here at some point
    #bindsym f exec thunar, mode "default"
    bindsym q exec --no-startup-id qutebrowser, mode "default"
    bindsym x exec --no-startup-id i3-sensible-terminal, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

mode "$design" {
    bindsym d exec --no-startup-id wpg -m, mode "default"
    bindsym d exec --no-startup-id wpg, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

mode "$window" {
    bindsym w exec --no-startup-id rofi -show window, mode "default"
    bindsym Shift+d kill, mode "default"
    bindsym l resize shrink width 10 px or 10 ppt
    bindsym j resize grow height 10 px or 10 ppt
    bindsym k resize shrink height 10 px or 10 ppt
    bindsym h resize grow width 10 px or 10 ppt

    # change container layout (stacked, tabbed, toggle split)
    # bindsym $mod+s layout stacking
    bindsym $mod+Shift+t layout tabbed, mode "default"
    bindsym $mod+Shift+e layout toggle split, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"

}

mode "$windowmanager" {
    # split in horizontal orientation
    bindsym s split v, mode "default"

    # split in vertical orientation
    bindsym v split h, mode "default"

    bindsym g mode "$mode_gaps"
    #bindsym e exec emacsclient ~/.config/i3/config, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"

    bindsym q exec --no-startup-id ~/.config/i3/displayselect, mode "default"
}

mode "$group" {
    # switch to workspace
    bindsym a workspace 1, mode "default"
    bindsym s workspace 2, mode "default"
    bindsym d workspace 3, mode "default"
    bindsym f workspace 4, mode "default"
    bindsym g workspace 5, mode "default"
    bindsym h workspace 6, mode "default"
    bindsym j workspace 7, mode "default"
    bindsym k workspace 8, mode "default"
    bindsym l workspace 9, mode "default"
    bindsym ö workspace 10, mode "default"
    bindsym Control+l workspace next, mode "default"
    bindsym Control+h workspace prev, mode "default"
    bindsym l workspace next
    bindsym h workspace prev

    # move focused container to workspace
    bindsym Shift+a move container to workspace 1, mode "default"
    bindsym Shift+s move container to workspace 2, mode "default"
    bindsym Shift+d move container to workspace 3, mode "default"
    bindsym Shift+f move container to workspace 4, mode "default"
    bindsym Shift+g move container to workspace 5, mode "default"
    bindsym Shift+h move container to workspace 6, mode "default"
    bindsym Shift+j move container to workspace 7, mode "default"
    bindsym Shift+k move container to workspace 8, mode "default"
    bindsym Shift+l move container to workspace 9, mode "default"
    bindsym Shift+ö move container to workspace 10, mode "default"
    bindsym Control+Shift+l move container to workspace next
    bindsym Control+Shift+h move container to workspace prev
    bindsym Shift+l move container to workspace next, mode "default"
    bindsym Shift+h move container to workspace prev, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

mode "$sound" {
    bindsym k exec --no-startup-id pactl set-sink-volume 0 -- +10% && killall -SIGUSR1 i3status
    bindsym j exec --no-startup-id pactl set-sink-volume 0 -- -10% && killall -SIGUSR1 i3status
    bindsym m exec --no-startup-id pactl set-sink-mute 0 toggle, mode "default" # mute sound

    bindsym g exec --no-startup-id playerctl play, mode "default"
    bindsym s exec --no-startup-id playerctl pause, mode "default"
    bindsym n exec --no-startup-id playerctl next, mode "default"
    bindsym p exec --no-startup-id playerctl previous, mode "default"
    bindsym e exec --no-startup-id pavucontrol-qt, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
# Media player controls
bindsym XF86AudioPlay exec --no-startup-id playerctl play
bindsym XF86AudioPause exec --no-startup-id playerctl pause
bindsym XF86AudioNext exec --no-startup-id playerctl next
bindsym XF86AudioPrev exec --no-startup-id playerctl previous
# Sreen brightness controls
bindsym XF86MonBrightnessUp exec --no-startup-id xbacklight -inc 20 # increase screen brightness
bindsym XF86MonBrightnessDown exec --no-startup-id xbacklight -dec 20 # decrease screen brightness
# sound
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 -- +10% && killall -SIGUSR1 i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -- -10% && killall -SIGUSR1 i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound
# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod


# class                 border    backgr.   text    indicator child_border
client.focused          $coloract $coloract $white  $split $coloract
client.focused_inactive $coloract $coloract $white  $split $coloract
client.unfocused        $colorin  $colorin  #888888 $split $colorin
client.urgent           #2f343a   #900000   $white  #900000 #900000
client.placeholder      #000000   #0c0c0c   $white  #000000 #0c0c0c

client.background       #ffffff


# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        status_command i3status
}


#exec --no-startup-id "setxkbmap -option caps:escape"
exec --no-startup-id exec volumeicon
#exec --no-startup-id exec compton
exec --no-startup-id emacs
exec --no-startup-id xmodmap ~/.config/i3/xmodmaprc

#exec_always --no-startup-id exec ~/Code/scripts/polybar.sh &


# Resize gaps
set $mode_gaps Gaps: (o)uter, (i)nner, (h)orizontal, (v)ertical, (t)op, (r)ight, (b)ottom, (l)eft
set $mode_gaps_outer Outer Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_inner Inner Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_horiz Horizontal Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_verti Vertical Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_top Top Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_right Right Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_bottom Bottom Gaps: +|-|0 (local), Shift + +|-|0 (global)
set $mode_gaps_left Left Gaps: +|-|0 (local), Shift + +|-|0 (global)

mode "$mode_gaps" {
        bindsym o      mode "$mode_gaps_outer"
        bindsym i      mode "$mode_gaps_inner"
        bindsym h      mode "$mode_gaps_horiz"
        bindsym v      mode "$mode_gaps_verti"
        bindsym t      mode "$mode_gaps_top"
        bindsym r      mode "$mode_gaps_right"
        bindsym b      mode "$mode_gaps_bottom"
        bindsym l      mode "$mode_gaps_left"
        bindsym Return mode "$mode_gaps"
        bindsym Escape mode "default"
}

mode "$mode_gaps_outer" {
        bindsym plus  gaps outer current plus 5
        bindsym minus gaps outer current minus 5
        bindsym 0     gaps outer current set 0

        bindsym Shift+plus  gaps outer all plus 5
        bindsym Shift+minus gaps outer all minus 5
        bindsym Shift+0     gaps outer all set 0

        bindsym Return mode "$mode_gaps"
        bindsym Escape mode "default"
}
mode "$mode_gaps_inner" {
        bindsym plus  gaps inner current plus 5
        bindsym minus gaps inner current minus 5
        bindsym 0     gaps inner current set 0

        bindsym Shift+plus  gaps inner all plus 5
        bindsym Shift+minus gaps inner all minus 5
        bindsym Shift+0     gaps inner all set 0

        bindsym Return mode "$mode_gaps"
        bindsym Escape mode "default"
}
mode "$mode_gaps_horiz" {
        bindsym plus  gaps horizontal current plus 5
        bindsym minus gaps horizontal current minus 5
        bindsym 0     gaps horizontal current set 0

        bindsym Shift+plus  gaps horizontal all plus 5
        bindsym Shift+minus gaps horizontal all minus 5
        bindsym Shift+0     gaps horizontal all set 0

        bindsym Return mode "$mode_gaps"
        bindsym Escape mode "default"
}
mode "$mode_gaps_verti" {
        bindsym plus  gaps vertical current plus 5
        bindsym minus gaps vertical current minus 5
        bindsym 0     gaps vertical current set 0

        bindsym Shift+plus  gaps vertical all plus 5
        bindsym Shift+minus gaps vertical all minus 5
        bindsym Shift+0     gaps vertical all set 0

        bindsym Return mode "$mode_gaps"
        bindsym Escape mode "default"
}
mode "$mode_gaps_top" {
        bindsym plus  gaps top current plus 5
        bindsym minus gaps top current minus 5
        bindsym 0     gaps top current set 0

        bindsym Shift+plus  gaps top all plus 5
        bindsym Shift+minus gaps top all minus 5
        bindsym Shift+0     gaps top all set 0

        bindsym Return mode "$mode_gaps"
        bindsym Escape mode "default"
}
mode "$mode_gaps_right" {
        bindsym plus  gaps right current plus 5
        bindsym minus gaps right current minus 5
        bindsym 0     gaps right current set 0

        bindsym Shift+plus  gaps right all plus 5
        bindsym Shift+minus gaps right all minus 5
        bindsym Shift+0     gaps right all set 0

        bindsym Return mode "$mode_gaps"
        bindsym Escape mode "default"
}
mode "$mode_gaps_bottom" {
        bindsym plus  gaps bottom current plus 5
        bindsym minus gaps bottom current minus 5
        bindsym 0     gaps bottom current set 0

        bindsym Shift+plus  gaps bottom all plus 5
        bindsym Shift+minus gaps bottom all minus 5
        bindsym Shift+0     gaps bottom all set 0

        bindsym Return mode "$mode_gaps"
        bindsym Escape mode "default"
}
mode "$mode_gaps_left" {
        bindsym plus  gaps left current plus 5
        bindsym minus gaps left current minus 5
        bindsym 0     gaps left current set 0

        bindsym Shift+plus  gaps left all plus 5
        bindsym Shift+minus gaps left all minus 5
        bindsym Shift+0     gaps left all set 0

        bindsym Return mode "$mode_gaps"
        bindsym Escape mode "default"
}