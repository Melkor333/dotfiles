HM_PATH="https://github.com/rycee/home-manager/archive/release-$(cat /etc/nixos/configuration.nix | grep stateVersion | cut -d '"' -f 2)".tar.gz

nix-shell $HM_PATH -A install

create_links.sh
