DOTFILESDIR="/home/sam/.config/nixpkgs/dotfiles/"
LINK=true
cd $DOTFILESDIR

# Get all checked in files with their proper path
files=`find . | cut -d '/' -f 2- | grep '/'`

echo "{pkgs, ...}:
{" > default.nix

for file in $files; do
  echo "  home.file.\"${file}\".source = \"${DOTFILESDIR}${file}\";" >> default.nix
  if $LINK && [ ! -e "$HOME/${file}" ]; then
    if [ ! -d ${file} ]; then
      echo "create links for $file"
      echo "create folder $HOME/${file%/*}"
      mkdir -p "$HOME/${file%/*}"
      ln -s "${DOTFILESDIR}${file}" "$HOME/${file}"
    fi
  fi
done
echo "}" >> default.nix
