#/bin/env bash

DOTFILESDIR="/home/sam/.config/nixpkgs/dotfiles"

if [[ -h "$1" ]]; then
  echo "Can't copy a symlink"
  exit 1
fi

f=`realpath -- "$1"`
[ ! -x "$(command -v whiptail)" ] && "whiptail must be available (package called 'newt')" && exit 1

cd $DOTFILESDIR

[[ ! "$f" =~ "$HOME/" ]] && echo "please provide a file in your home directory" && exit 1


function linkfile() {
  locfile="${DOTFILESDIR}${1#$HOME}"
  echo $locfile

  if [[ -e "$locfile" ]]; then
    echo "File already exists:"
    echo "$locfile"
    return
  fi

  echo creating folder "${locfile%/*}"
  mkdir -p "${locfile%/*}"
  echo "copy file ${1} to $locfile"
  mv "${1}" "$locfile"
  echo "linking ${1} to $locfile"
  ln -s "$locfile" "$1"
}

function linkfolder() {

  # Read all file in the directory into an array
  files=()
  while read file; do
    files+=("${file}" "" "yes")
    echo "found $file"
  done <<< `find "$1" -type f`

  echo "$files"

  files=`whiptail --title "select files" --checklist "Select the files you want to move" 0 0 15 "${files[@]}" 3>&1 1>&2 2>&3`
  # delete unnecessary outer quotes
  files="${files//\"/}"

  for file in $files; do
    linkfile $file
  done
}

if [[ -d "$f" ]]; then
  linkfolder "$f"
elif [[ -f "$f" ]]; then
  linkfile "$f"
else
  echo "Please provide a valid file or folder"
fi
