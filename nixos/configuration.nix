#ALL Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{

  # For Package maintenance
  nix.useSandbox = true;

  imports =
    [ # Include the results of the hardware scan.
      <nixos-hardware/apple/macbook-air/6>
      ./hardware-configuration.nix
      ./mopidy.nix
    ];

  # garbage collect
  nix.gc.automatic = true;
  nix.gc.dates = "weekly";
  nix.gc.options = "--max-freed $((64 * 1024**3))";

  fileSystems."/mnt/nas" = {
    device = "//DNS-320/Volume_1";
    fsType = "cifs";
    options = [ "vers=1.0" "x-systemd.automount" "noauto" "hard" "intr" "noexec" "credentials=/etc/nixos/smd.cred" ];
  };

  # Hardware stuff
  sound.enable = true;
  hardware.pulseaudio = {
    # For steam
    support32Bit = true;
    package = pkgs.pulseaudioFull.override { jackaudioSupport = true; };
    enable = true;
    # necessary for mopidy to work
    extraConfig = ''
load-module module-native-protocol-tcp auth-ip-acl=127.0.0.1
'';
  };
  # For steam
  hardware.opengl.driSupport32Bit = true;

  boot.extraModprobeConfig = ''
      options hid_apple fnmode=2
    '';

  # boot.kernelPatches = [ {
  #   name = "thunderbolt";
  #   patch = null;
  #   extraConfig = ''
  #     THUNDERBOLT y
  #     HOTPLUG_PCI y
  #     HOTPLUG_PCI_ACPI y
  #   '';
  #} ];
  services.acpid.enable = true;


  nixpkgs.config.packageOverrides = pkgs: {
    # linux = pkgs.linuxPackages.override {
    #   extraConfig = ''
    #     THUNDERBOLT y
    #     CONFIG_HOTPLUG_PCI y
    #     CONFIG_HOTPLUG_PCI_ACPI y
    #   '';
    # };
    qtile = pkgs.qtile.overrideAttrs (oldAttrs: { src = /home/sam/projects/qtile; });
  };

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  # Since it will build Virtualbox manually, i don't really want to use it...
  # virtualisation.virtualbox.host = {
  #   enable = true;
  #   enableExtensionPack = true;
  # };

  virtualisation.libvirtd = {
    enable = true;
    onShutdown = "suspend";
    qemuRunAsRoot = false;
  };


  networking.hostName = "Waluigi"; # Define your hostname.
  networking.networkmanager.enable = true;
  # networking.wireless = {
  #   userControlled.enable = true;
  # };
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "sg";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Europe/Zurich";

  nixpkgs.config.allowUnfree = true;
  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # mgmt
    ## network
    wget
    curl
    sshfs
    nmap
    rsync

    ## process
    psmisc # things like killall
    htop

    ## Hardware
    lshw
    usbutils
    thunderbolt

    ## file
    xfce.thunar
    file
    zip
    unzip

    ## project
    git

    ## shell
    screen
    powerline-fonts

    ## virtualisation
    virtmanager

    ## backup
    borgbackup

    # editor / viewer
    neovim # Config manual in .config/nvim/
    emacs
    # oni (packet must exist first)
    ## PDF viewer
    #apvlv

    # Python
    python36Packages.virtualenv
    # Necessary for emacs anaconda
    python36Packages.setuptools

    # xorg
    xorg.setxkbmap
    gtk3
    #wpgtk

    # musicstuff
    lxqt.pavucontrol-qt
    jack2Full
    # this is necessecary for midi! (and the jack startupscript)
    a2jmidid
    qjackctl
    #libjack2
    ardour
    audacity
    mixxx
    cadence
    ## tuner
    lingot
    rakarrack
    jack_capture
    guitarix

    ## plugins & instruments
    zynaddsubfx
    klick
    jack_rack
    zam-plugins
    x42-plugins
    yoshimi
    swh_lv2
    caps
    soundfont-fluid
    samplv1
    qsampler
    hydrogen
    drumgizmo
    calf

    # random stuff
    ## mistype
    gti
    sl
  ];
  # nixpkgs.overlays = [ (self: super: with self; {
  #     qtile = super.qtile.override {
  #       python27Packages = python36Packages;
  #     };
  # }) ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.bash.enableCompletion = true;
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  fonts.fonts = with pkgs; [
    powerline-fonts
  ];

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = false;

  # Enable the X11 windowing system.
  services.xserver =
  {
    enable = true;
    desktopManager = {
      default = "none";
      xterm.enable = true;
    };

    windowManager = {
      qtile.enable = true;
      i3.enable = true;
      i3.package = pkgs.i3-gaps;
    };

    displayManager.sddm.enable = true;
    layout = "ch";
    xkbOptions = "caps:none";
  };
  # This is necessary for wpgtk
  programs.dconf.enable = true;
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Audio lock configs
  security.pam.loginLimits = [
    { domain = "@audio"; item = "memlock"; type = "-"; value = "unlimited"; }
    { domain = "@audio"; item = "rtprio"; type = "-"; value = "99"; }
    { domain = "@audio"; item = "nofile"; type = "soft"; value = "99999"; }
    { domain = "@audio"; item = "nofile"; type = "hard"; value = "99999"; }
  ];
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.sam = {
    isNormalUser = true;
    description = "Samuel Ruprecht";
    uid = 1000;
    hashedPassword = "$6$W1EiUVW6$SCZligh3CBEEehkQ60oasW4UkeI3P2DX52Nc1ZEGyP1zMNX6BfVl1OPHu/q.K9lwYH5jJmJmuDyeySoB0YkvU/";
    extraGroups = ["wheel" "networkmanager" "libvirtd" "audio"];
  };
  #users.users.sam.packages = [ pkgs.steam ];
 
  # Plugin directories
  environment.shellInit = ''
    export VST_PATH=/nix/var/nix/profiles/default/lib/vst:/var/run/current-system/sw/lib/vst:~/.vst
    export LXVST_PATH=/nix/var/nix/profiles/default/lib/lxvst:/var/run/current-system/sw/lib/lxvst:~/.lxvst
    export LADSPA_PATH=/nix/var/nix/profiles/default/lib/ladspa:/var/run/current-system/sw/lib/ladspa:~/.ladspa
    export LV2_PATH=/nix/var/nix/profiles/default/lib/lv2:/var/run/current-system/sw/lib/lv2:~/.lv2
    export DSSI_PATH=/nix/var/nix/profiles/default/lib/dssi:/var/run/current-system/sw/lib/dssi:~/.dssi
  '';

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "18.09"; # Did you read the comment?

}
